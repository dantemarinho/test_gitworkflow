<?php
// abrir conexao
$conecta = mysqli_connect("localhost", "root", "", "andes"); // server, user, "", BD

$selecao = "SELECT nomeproduto, precounitario, imagempequena FROM produtos";
$produtos = mysqli_query($conecta, $selecao);

$retorno = array();
while($linha = mysqli_fetch_object($produtos)) {
    $retorno[] = $linha;
}

// retorno em JSON
// echo json_encode($retorno);

// ou retorno de forma formatada
header('Content-Type: application/json');
echo json_encode($retorno, JSON_PRETTY_PRINT);
// https://stackoverflow.com/questions/6054033/pretty-printing-json-with-php


//fechar conect
mysqli_close($conecta);
?>